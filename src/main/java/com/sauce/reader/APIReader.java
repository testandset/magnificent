package com.sauce.reader;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.sauce.counter.StatsCounter;

/**
 * API reader
 */
public class APIReader implements Runnable {
	
	StatsCounter statsCounter;
	
	private static final String MAGNIFICANT = "Magnificent!";

	/**
	 * Used for thread signalling
	 */
	protected boolean isAllProcessed = false;

	/**
	 * URL to read it data from
	 */
	private String URL;

	/**
	 * Response handling from API.
	 * status.
	 */
	private ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
		@Override
		public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
			int status = response.getStatusLine().getStatusCode();
			if (status == 200) {
				HttpEntity entity = response.getEntity();
				return EntityUtils.toString(entity);
			}

			return null;
		}
	};

	/**
	 * Set url
	 * @param url
	 */
	public void setURL(String url) {
		this.URL = url;
	}

	public void setStatCounter(StatsCounter statsCounter) {
		this.statsCounter = statsCounter;
	}
	/**
	 * Runnable interface method, to pass to a thread.
	 * Continuously reads a response from API.
	 */
	@Override
	public void run() {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget;
		String response;
		
		try {
			httpget = new HttpGet(URL);	
			
			while(true){
				response = httpclient.execute(httpget, responseHandler);

				processResponse(response);
				
				// Lets hit it three times a minute;
				Thread.sleep(20000);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void processResponse(String response) {
		if(MAGNIFICANT.equals(response)) {
			statsCounter.addHit();
		} else {
			statsCounter.addMiss();
		}
	}

}
