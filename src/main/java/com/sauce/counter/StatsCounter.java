package com.sauce.counter;

public interface StatsCounter {
	
	public void addHit();
	
	public void addMiss();
	
	public long getHits();
	
	public long getMisses();

}
