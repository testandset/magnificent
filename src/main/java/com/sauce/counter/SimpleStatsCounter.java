package com.sauce.counter;

public class SimpleStatsCounter implements StatsCounter {	
	
	private long hits = 0;
	
	private long misses = 0;	
	
	public void addHit() {
		hits++;
	}
	
	public long getHits() {		
		return hits;
	}
	
	public void addMiss() {
		misses++;
	}
	
	public long getMisses() {
		return misses;
	}

}
