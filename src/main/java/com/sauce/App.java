package com.sauce;

import com.sauce.counter.SimpleStatsCounter;
import com.sauce.counter.StatsCounter;
import com.sauce.reader.APIReader;
import com.sauce.writer.StatsWriter;
import com.sauce.writer.TempFileStatsWriter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){
    	StatsCounter statsCounter = new SimpleStatsCounter();
    	
        APIReader monitor = new APIReader();        
        monitor.setURL("http://localhost:12345/");
        monitor.setStatCounter(statsCounter);
        
        StatsWriter writer = new TempFileStatsWriter();
        writer.setStatCounter(statsCounter);
        
        Thread writeThread = new Thread(writer);        
        Thread monitorThread = new Thread(monitor);
        
        // Can't work out why when I set it to as Deamon its not running?
        //writeThread.setDaemon(true);
        //monitorThread.setDaemon(true);

        monitorThread.start();
        writeThread.start();
    }
}
