package com.sauce.writer;

import com.sauce.counter.StatsCounter;

public interface StatsWriter extends Runnable {
	
	public void setStatCounter(StatsCounter counter);
	
	public void writeStats();
}
