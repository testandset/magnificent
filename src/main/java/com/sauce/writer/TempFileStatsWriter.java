package com.sauce.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.sauce.counter.StatsCounter;

public class TempFileStatsWriter implements StatsWriter{

	private StatsCounter statsCounter;
	
	private final String TEMP_DIR = System.getProperty("java.io.tmpdir");

	@Override
	public void setStatCounter(StatsCounter counter) {
		this.statsCounter = counter;		
	}

	@Override
	public void writeStats() {
		File tempFile = null;
        BufferedWriter writer = null;
        try {
            //tempFile = File.createTempFile("stats", ".tmp");
        	tempFile = new File(TEMP_DIR + File.separator + "stats.txt");
            writer = new BufferedWriter(new FileWriter(tempFile));
            writer.write(getHealth());
        } catch (IOException e) {
            // TODO - sort this
        } finally {
            try{
                if(writer != null) writer.close();
            }catch(Exception ex){
            	// TODO - sort this
            }
        }		
	}
	
	protected String getHealth() {
		double health = (statsCounter.getHits() / (double)(statsCounter.getHits() + statsCounter.getMisses())) * 100;
		return String.format("%1$,.2f", health);
	}

	@Override
	public void run() {
		while(true) {			
			try {
				// write every minute
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				// TODO - sort this
			}
			
			writeStats();
		}
		
	}

}
